package ord2019.part1and2;

public class Patient {

    // TODO 2a: Add fields, constructors, and methods here

    /**
     * Indicates if this patient has conditions that needs to be treated.
     * 
     * @return true if this patient has conditions that needs to be treated, false
     *         otherwise.
     */
    public boolean requiresTreatment() {
        // TODO 2a
        return false;
    }

}