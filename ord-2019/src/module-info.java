/**
 * @author hal
 *
 */
open module ord2019 {
	requires javafx.base;
	requires javafx.controls;
	requires javafx.fxml;
	requires javafx.graphics;
	requires junit;
}
